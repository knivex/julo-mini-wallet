Mini Wallet
===========
JULO Technical Assessment
-------------------------

### Prerequisite
Make sure Java 8 (or higher) is installed on your machine.

### Running the app
1. In terminal / command prompt, navigate to project root (the directory where this file is located).
2. Make sure the port `8080` is not being used.
3. For UNIX, run the command `./mvnw spring-boot:run`, or for Windows `mvnw.cmd spring-boot:run`.
4. When the last line of the output is similar to
    `Started MiniWalletApplication in xxx seconds (JVM running for xxx)`,
    then the server is ready to be accessed on `localhost:8080/api/v1/init`.
5. To view & run query on the active database, open `localhost:8080/h2-console/` and input the following values:

   - JDBC URL: `jdbc:h2:file:./mini_wallet`
   - User Name: `sa`
   - Password: `password`

6. Press `Ctrl/Cmd + C` to stop the server.

### Cleanup
- Inside the working directory, a database file named `mini_wallet.mv.db` will be created.  
  This file can be deleted if you need a fresh database. 

- Maven saves downloaded files in the following default location:

  - UNIX: `~/.m2`
  - Windows: `C:\Users\<username>\.m2`

  These directories can be safely deleted when Maven is not used anymore.
