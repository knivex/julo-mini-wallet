package net.knivex.mini_wallet.controller;

import lombok.NonNull;
import lombok.val;
import net.knivex.mini_wallet.dto.*;
import net.knivex.mini_wallet.exception.*;
import net.knivex.mini_wallet.model.*;
import net.knivex.mini_wallet.service.CustomerService;
import net.knivex.mini_wallet.service.WalletService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class MiniWalletControllerTest {
    private final CustomerService mockCustomerService = mock(CustomerService.class);
    private final WalletService mockWalletService = mock(WalletService.class);

    private final MiniWalletController controller = new MiniWalletController(
            mockCustomerService,
            mockWalletService
    );

    // ~ init ~
    @Test
    void givenCustomerId_whenInit_thenReturnToken() {
        // given
        val token = "tokentokentoken";

        when(mockCustomerService.login(any()))
                .thenReturn(token);

        // when
        val response = controller.init("customer id");
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(Token.class);

        val tokenObj = (Token) body.getData();
        assertThat(tokenObj.getToken()).isEqualTo(token);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void givenBlankCustomerId_whenInit_thenReturnError(String customerId) {
        // when
        val response = controller.init(customerId);
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(InvalidParamError.class);

        verifyNoInteractions(mockCustomerService);
    }

    // ~ enableMyWallet ~
    @Test
    void givenValidWallet_whenEnableMyWallet_thenReturnWallet() throws Exception {
        // given
        val customerId = "987654";
        val auth = generateAuth(customerId);
        val wallet = generateWallet(customerId);

        when(mockWalletService.enable(customerId))
                .thenReturn(wallet);

        // when
        val response = controller.enableMyWallet(auth);
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isEqualTo(wallet);
    }

    @Test
    void givenEnabledWallet_whenEnableMyWallet_thenReturnError() throws Exception {
        // given
        val customerId = "984267";
        val auth = generateAuth(customerId);

        when(mockWalletService.enable(customerId))
                .thenThrow(new EnabledWalletException(""));

        // when
        val response = controller.enableMyWallet(auth);
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    // ~ disableMyWallet ~
    @Test
    void givenSuccessfullyDisabledWallet_whenDisableMyWallet_thenReturnWallet() throws Exception {
        // given
        val customerId = "184937";
        val auth = generateAuth(customerId);
        val wallet = generateWallet(customerId);
        wallet.disable();

        when(mockWalletService.disable(customerId))
                .thenReturn(wallet);

        // when
        val response = controller.disableMyWallet(auth, "true");
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isEqualTo(wallet);
    }

    @Test
    void givenWalletNotFound_whenDisableMyWallet_thenReturnError() throws Exception {
        // given
        val customerId = "916735";
        val auth = generateAuth(customerId);

        when(mockWalletService.disable(customerId))
                .thenThrow(new WalletNotFoundException(""));

        // when
        val response = controller.disableMyWallet(auth, "true");
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    @Test
    void givenIsDisabledDoesNotEqualToTrue_whenDisableMyWallet_thenReturnError() {
        // given
        val customerId = "972465";
        val auth = generateAuth(customerId);

        // when
        val response = controller.disableMyWallet(auth, "invalid");
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(InvalidParamError.class);

        verifyNoInteractions(mockWalletService);
    }

    // ~ viewMyWalletBalance ~
    @Test
    void givenValidWallet_whenViewMyWalletBalance_thenReturnWallet() {
        // given
        val customerId = "784512";
        val auth = generateAuth(customerId);
        val wallet = generateWallet(customerId);

        when(mockWalletService.find(customerId))
                .thenReturn(Optional.of(wallet));

        // when
        val response = controller.viewMyWalletBalance(auth);
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isEqualTo(wallet);
    }

    @Test
    void givenWalletIsNotFound_whenViewMyWalletBalance_thenReturnError() {
        // given
        val customerId = "875421";
        val auth = generateAuth(customerId);

        when(mockWalletService.find(customerId))
                .thenReturn(Optional.empty());

        // when
        val response = controller.viewMyWalletBalance(auth);
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    @Test
    void givenDisabledWallet_whenViewMyWalletBalance_thenReturnError() {
        // given
        val customerId = "785412";
        val auth = generateAuth(customerId);
        val wallet = generateWallet(customerId);
        wallet.disable();

        when(mockWalletService.find(customerId))
                .thenReturn(Optional.of(wallet));

        // when
        val response = controller.viewMyWalletBalance(auth);
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    // ~ addVirtualMoneyToMyWallet ~
    @Test
    void givenSuccessfulDeposit_whenAddVirtualMoneyToMyWallet_returnDepositInfo() throws Exception {
        // given
        val customerId = "142578";
        val amount = 31000L;
        val refId = UUID.randomUUID();

        val auth = generateAuth(customerId);
        val depositTrx = new DepositTrx();

        when(mockWalletService.deposit(customerId, amount, refId))
                .thenReturn(depositTrx);

        // when
        val response = controller.addVirtualMoneyToMyWallet(auth, amount, refId);
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(DepositTrx.class);
    }

    @Test
    void givenWalletNotFound_whenAddVirtualMoneyToMyWallet_returnError() throws Exception {
        // given
        val auth = generateAuth("251478");
        when(mockWalletService.deposit(any(), anyLong(), any()))
                .thenThrow(new WalletNotFoundException(""));

        // when
        val response = controller.addVirtualMoneyToMyWallet(auth, 10, UUID.randomUUID());
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    @Test
    void givenDisabledWallet_whenAddVirtualMoneyToMyWallet_returnError() throws Exception {
        // given
        val auth = generateAuth("412578");

        when(mockWalletService.deposit(any(), anyLong(), any()))
                .thenThrow(new DisabledWalletException(""));

        // when
        val response = controller.addVirtualMoneyToMyWallet(auth, 10, UUID.randomUUID());
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    @Test
    void givenNegativeAmount_whenAddVirtualMoneyToMyWallet_returnError() throws Exception {
        // given
        val auth = generateAuth("145278");

        when(mockWalletService.deposit(any(), anyLong(), any()))
                .thenThrow(new NegativeAmountException(""));

        // when
        val response = controller.addVirtualMoneyToMyWallet(auth, 10, UUID.randomUUID());
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    @Test
    void givenDuplicateReference_whenAddVirtualMoneyToMyWallet_returnError() throws Exception {
        // given
        val auth = generateAuth("254178");

        when(mockWalletService.deposit(any(), anyLong(), any()))
                .thenThrow(new DuplicateReferenceException(""));

        // when
        val response = controller.addVirtualMoneyToMyWallet(auth, 10, UUID.randomUUID());
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    // ~ useVirtualMoneyFromMyWallet ~
    @Test
    void givenSuccessfulWithdrawal_whenUseVirtualMoneyFromMyWallet_thenReturnWithdrawalInfo() throws Exception {
        // given
        val customerId = "976485";
        val amount = 31000L;
        val refId = UUID.randomUUID();

        val auth = generateAuth(customerId);
        val withdrawalTrx = new WithdrawalTrx();

        when(mockWalletService.withdraw(customerId, amount, refId))
                .thenReturn(withdrawalTrx);

        // when
        val response = controller.useVirtualMoneyFromMyWallet(auth, amount, refId);
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(WithdrawalTrx.class);
    }

    @Test
    void givenWalletNotFound_whenUseVirtualMoneyFromMyWallet_thenReturnError() throws Exception {
        // given
        val auth = generateAuth("354861");

        when(mockWalletService.withdraw(any(), anyLong(), any()))
                .thenThrow(new WalletNotFoundException(""));

        // when
        val response = controller.useVirtualMoneyFromMyWallet(auth, 10, UUID.randomUUID());
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    @Test
    void givenDisabledWallet_whenUseVirtualMoneyFromMyWallet_thenReturnError() throws Exception {
        // given
        val auth = generateAuth("948875");

        when(mockWalletService.withdraw(any(), anyLong(), any()))
                .thenThrow(new DisabledWalletException(""));

        // when
        val response = controller.useVirtualMoneyFromMyWallet(auth, 10, UUID.randomUUID());
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    @Test
    void givenNegativeAmount_whenUseVirtualMoneyFromMyWallet_thenReturnError() throws Exception {
        // given
        val auth = generateAuth("164825");
        when(mockWalletService.withdraw(any(), anyLong(), any()))
                .thenThrow(new NegativeAmountException(""));

        // when
        val response = controller.useVirtualMoneyFromMyWallet(auth, 10, UUID.randomUUID());
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    @Test
    void givenAmountExceeded_whenUseVirtualMoneyFromMyWallet_thenReturnError() throws Exception {
        // given
        val auth = generateAuth("164825");

        when(mockWalletService.withdraw(any(), anyLong(), any()))
                .thenThrow(new AmountExceededException(""));

        // when
        val response = controller.useVirtualMoneyFromMyWallet(auth, 10, UUID.randomUUID());
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    @Test
    void givenDuplicateReference_whenUseVirtualMoneyFromMyWallet_thenReturnError() throws Exception {
        // given
        val auth = generateAuth("915648");

        when(mockWalletService.withdraw(any(), anyLong(), any()))
                .thenThrow(new DuplicateReferenceException(""));

        // when
        val response = controller.useVirtualMoneyFromMyWallet(auth, 10, UUID.randomUUID());
        val body = response.getBody();

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(body).isNotNull();
        assertThat(body.getData()).isInstanceOf(GeneralError.class);
    }

    private Authentication generateAuth(@NonNull String customerId) {
        return new UsernamePasswordAuthenticationToken(customerId, "password", Collections.emptyList());
    }

    private Wallet generateWallet(String ownedBy) {
        val wallet = new Wallet();
        wallet.setOwnedBy(ownedBy);

        return wallet;
    }
}
