package net.knivex.mini_wallet.service;

import lombok.val;
import net.knivex.mini_wallet.exception.*;
import net.knivex.mini_wallet.model.Wallet;
import net.knivex.mini_wallet.repo.TransactionRepo;
import net.knivex.mini_wallet.repo.WalletRepo;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class WalletServiceTest {
    private final WalletRepo mockWalletRepo = mock(WalletRepo.class);
    private final TransactionRepo mockTransactionRepo = mock(TransactionRepo.class);

    private final WalletService walletService = new WalletService(mockWalletRepo, mockTransactionRepo);

    // ~ enable() ~
    @Test
    void givenNewCustomer_whenEnable_thenReturnWallet() throws Exception {
        // given
        val customerId = "123456";

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.empty());

        // when
        val wallet = walletService.enable(customerId);

        // then
        assertThat(wallet).isNotNull();
        assertThat(wallet.getOwnedBy()).isEqualTo(customerId);
        assertThat(wallet.getBalance()).isEqualTo(0L);

        verify(mockWalletRepo, times(1)).save(wallet);
    }

    @Test
    void givenExistingCustomer_whenEnable_thenThrowWalletException() {
        // given
        val customerId = "321654";
        val wallet = generateWallet(customerId);

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(wallet));

        // when
        val exc = catchThrowableOfType( () -> walletService.enable(customerId) , EnabledWalletException.class );

        // then
        assertThat(exc).hasMessageContaining(customerId);
    }

    @Test
    void givenDisabledWallet_whenEnable_thenReturnWallet() throws Exception {
        // given
        val customerId = "658713";
        val disabledWallet = generateWallet(customerId);
        disabledWallet.disable();

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(disabledWallet));

        // when
        val wallet = walletService.enable(customerId);

        // then
        assertThat(wallet).isNotNull();
        assertThat(wallet.isEnabled()).isTrue();

        verify(mockWalletRepo, times(1)).save(wallet);
    }

    @Test
    void givenDisabledWallet_whenEnable_thenBalanceDoesNotChange() throws Exception {
        // given
        val customerId = "798315";
        val expectedBalance = 100L;
        val disabledWallet = generateWallet(customerId);

        disabledWallet.setBalance(expectedBalance);
        disabledWallet.disable();

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(disabledWallet));

        // when
        val wallet = walletService.enable(customerId);

        // then
        assertThat(wallet).isNotNull();
        assertThat(wallet.isEnabled()).isTrue();
        assertThat(wallet.getBalance()).isEqualTo(expectedBalance);

        verify(mockWalletRepo, times(1)).save(wallet);
    }

    // ~ disable() ~
    @Test
    void givenExistingCustomer_whenDisable_thenReturnDisabledWallet() throws Exception {
        // given
        val customerId = "987163";
        val enabledWallet = generateWallet(customerId);

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(enabledWallet));

        // when
        val wallet = walletService.disable(customerId);

        // then
        assertThat(wallet).isNotNull();
        assertThat(wallet.isEnabled()).isFalse();
        assertThat(wallet.getDisabledAt()).isNotNull();

        verify(mockWalletRepo, times(1)).save(wallet);
    }

    @Test
    void givenDisabledWallet_whenDisable_thenReturnDisabledWallet() throws Exception {
        // given
        val customerId = "987163";
        val disabledWallet = generateWallet(customerId);
        disabledWallet.disable();

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(disabledWallet));

        // when
        val wallet = walletService.disable(customerId);

        // then
        assertThat(wallet).isNotNull();
        assertThat(wallet.isEnabled()).isFalse();
        assertThat(wallet.getDisabledAt()).isNotNull();

        verify(mockWalletRepo, never()).save(any());
    }

    @Test
    void givenInvalidCustomer_whenDisable_thenThrowException() {
        // given
        val customerId = "987632";

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.empty());

        // when
        val exc = catchThrowableOfType( () -> walletService.disable(customerId) , WalletNotFoundException.class );

        // then
        assertThat(exc).hasMessageContaining(customerId);
    }

    // ~ find() ~
    @Test
    void givenExistingCustomer_whenFind_thenReturnWallet() {
        // given
        val customerId = "123987";
        val expectedWallet = generateWallet(customerId);

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(expectedWallet));

        // when
        val wallet = walletService.find(customerId);

        // then
        assertThat(wallet).containsSame(expectedWallet);
    }

    @Test
    void givenInvalidCustomer_whenFind_thenReturnEmptyOptional() {
        // given
        val customerId = "654783";

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.empty());

        // when
        val wallet = walletService.find(customerId);

        // then
        assertThat(wallet).isEmpty();
    }

    // ~ deposit() ~
    @Test
    void givenValidArguments_whenDeposit_thenBalanceIncreasedAndReturnDepositInfo() throws Exception {
        // given
        val customerId = "874521";
        val wallet = generateWallet(customerId);

        val amount = 10000L;
        val refId = UUID.randomUUID();

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(wallet));
        when(mockTransactionRepo.existsByReferenceId(refId))
                .thenReturn(false);

        // when
        val depositTrx = walletService.deposit(customerId, amount, refId);

        // then
        assertThat(depositTrx.getDepositedBy()).isEqualTo(customerId);
        assertThat(depositTrx.getAmount()).isEqualTo(amount);
        assertThat(depositTrx.getReferenceId()).isEqualTo(refId);

        assertThat(wallet.getBalance()).isEqualTo(amount);

        verify(mockWalletRepo, times(1)).save(wallet);
        verify(mockTransactionRepo, times(1)).save(depositTrx);
    }

    @Test
    void givenWalletIsDeposited_whenDeposit_thenBalanceIncreasedAndReturnDepositInfo() throws Exception {
        // given
        val customerId = "147258";
        val wallet = generateWallet(customerId);

        val initBalance = 10000L;
        val amount = 32000L;
        val refId = UUID.randomUUID();

        wallet.setBalance(initBalance);

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(wallet));
        when(mockTransactionRepo.existsByReferenceId(refId))
                .thenReturn(false);

        // when
        val depositTrx = walletService.deposit(customerId, amount, refId);

        // then
        assertThat(depositTrx.getDepositedBy()).isEqualTo(customerId);
        assertThat(depositTrx.getAmount()).isEqualTo(amount);
        assertThat(depositTrx.getReferenceId()).isEqualTo(refId);

        assertThat(wallet.getBalance()).isEqualTo(initBalance + amount);

        verify(mockWalletRepo, times(1)).save(wallet);
        verify(mockTransactionRepo, times(1)).save(depositTrx);
    }

    @Test
    void givenInvalidCustomerId_whenDeposit_thenThrowException() {
        // given
        val customerId = "741852";

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.empty());

        // when
        val exc = catchThrowableOfType(
                () -> walletService.deposit(customerId, 10000L, UUID.randomUUID()),
                WalletNotFoundException.class
        );

        // then
        assertThat(exc).hasMessageContaining(customerId);
    }

    @Test
    void givenDisabledWallet_whenDeposit_thenThrowException() {
        // given
        val customerId = "741852";
        val wallet = generateWallet(customerId);
        wallet.disable();

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(wallet));

        // when
        val exc = catchThrowableOfType(
                () -> walletService.deposit(customerId, 12300L, UUID.randomUUID()),
                DisabledWalletException.class
        );

        // then
        assertThat(exc).hasMessageContaining(customerId);
    }

    @Test
    void givenNegativeAmount_whenDeposit_thenThrowException() {
        // given
        val customerId = "741258";

        // when
        val exc = catchThrowableOfType(
                () -> walletService.deposit(customerId, -10000L, UUID.randomUUID()),
                NegativeAmountException.class
        );

        // then
        assertThat(exc).hasMessageContaining("negative");
    }

    @Test
    void givenDuplicateReferenceId_whenDeposit_thenThrowException() {
        val customerId = "741852";
        val wallet = generateWallet(customerId);
        val refId = UUID.randomUUID();

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(wallet));
        when(mockTransactionRepo.existsByReferenceId(refId))
                .thenReturn(true);

        // when
        val exc = catchThrowableOfType(
                () -> walletService.deposit(customerId, 200L, refId),
                DuplicateReferenceException.class
        );

        // then
        assertThat(exc).hasMessageContaining(refId.toString());
    }

    // ~ withdraw() ~
    @Test
    void givenValidArguments_whenWithdraw_thenBalanceDecreasedAndReturnWithdrawalInfo() throws Exception {
        // given
        val customerId = "134679";
        val wallet = generateWallet(customerId);

        val initBalance = 10000L;
        val withdrawnAmount = 3000L;
        val refId = UUID.randomUUID();

        wallet.setBalance(initBalance);

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(wallet));
        when(mockTransactionRepo.existsByReferenceId(refId))
                .thenReturn(false);

        // when
        val withdrawalTrx = walletService.withdraw(customerId, withdrawnAmount, refId);

        // then
        assertThat(withdrawalTrx.getWithdrawnBy()).isEqualTo(customerId);
        assertThat(withdrawalTrx.getAmount()).isEqualTo(withdrawnAmount);
        assertThat(withdrawalTrx.getReferenceId()).isEqualTo(refId);

        assertThat(wallet.getBalance()).isEqualTo(initBalance - withdrawnAmount);

        verify(mockWalletRepo, times(1)).save(wallet);
        verify(mockTransactionRepo, times(1)).save(withdrawalTrx);
    }

    @Test
    void givenInvalidCustomerId_whenWithdraw_thenThrowException() {
        // given
        val customerId = "467913";

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.empty());

        // when
        val exc = catchThrowableOfType(
                () -> walletService.withdraw(customerId, 10000L, UUID.randomUUID()),
                WalletNotFoundException.class
        );

        // then
        assertThat(exc).hasMessageContaining(customerId);
    }

    @Test
    void givenDisabledWallet_whenWithdraw_thenThrowException() {
        // given
        val customerId = "791346";
        val wallet = generateWallet(customerId);
        wallet.disable();

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(wallet));

        // when
        val exc = catchThrowableOfType(
                () -> walletService.withdraw(customerId, 12300L, UUID.randomUUID()),
                DisabledWalletException.class
        );

        // then
        assertThat(exc).hasMessageContaining(customerId);
    }

    @Test
    void givenNegativeAmount_whenWithdraw_thenThrowException() {
        // given
        val customerId = "794613";

        // when
        val exc = catchThrowableOfType(
                () -> walletService.withdraw(customerId, -10000L, UUID.randomUUID()),
                NegativeAmountException.class
        );

        // then
        assertThat(exc).hasMessageContaining("negative");
    }

    @Test
    void givenExceedingAmount_whenWithdraw_thenThrowException() {
        // given
        val customerId = "791346";
        val wallet = generateWallet(customerId);
        val withdrawAmount = 1940L;

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(wallet));

        // when
        val exc = catchThrowableOfType(
                () -> walletService.withdraw(customerId, withdrawAmount, UUID.randomUUID()),
                AmountExceededException.class
        );

        // then
        assertThat(exc).hasMessageContaining(Long.toString(withdrawAmount));
    }

    @Test
    void givenDuplicateReferenceId_whenWithdraw_thenThrowException() {
        // given
        val customerId = "316497";
        val wallet = generateWallet(customerId);
        val refId = UUID.randomUUID();

        wallet.setBalance(100L);

        when(mockWalletRepo.findByOwnedBy(customerId))
                .thenReturn(Optional.of(wallet));
        when(mockTransactionRepo.existsByReferenceId(refId))
                .thenReturn(true);

        // when
        val exc = catchThrowableOfType(
                () -> walletService.withdraw(customerId, 20L, refId),
                DuplicateReferenceException.class
        );

        // then
        assertThat(exc).hasMessageContaining(refId.toString());
    }

    private Wallet generateWallet(String ownedBy) {
        val wallet = new Wallet();
        wallet.setOwnedBy(ownedBy);

        return wallet;
    }
}
