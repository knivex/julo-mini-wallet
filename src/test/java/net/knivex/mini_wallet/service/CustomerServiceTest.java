package net.knivex.mini_wallet.service;

import lombok.val;
import net.knivex.mini_wallet.model.Customer;
import net.knivex.mini_wallet.repo.CustomerRepo;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class CustomerServiceTest {
    private final CustomerRepo mockCustomerRepo = mock(CustomerRepo.class);

    private final CustomerService customerService = new CustomerService(mockCustomerRepo);

    // ~ login() ~
    @Test
    void givenNewCustomerId_whenLogin_thenReturnRandomToken() {
        // given
        val customerId = "123456789";

        when(mockCustomerRepo.findById(customerId))
                .thenReturn(Optional.empty());

        // when
        val token = customerService.login(customerId);

        // then
        assertThat(token).isNotBlank();

        verify(mockCustomerRepo, times(1)).save(
                generateCustomer(customerId, token)
        );
    }

    @Test
    void givenExistingCustomerId_whenLogin_thenReturnSameToken() {
        // given
        val customerId = "123456789";
        val expectedToken = "tokentokentoken";

        val customer = generateCustomer(customerId, expectedToken);

        when(mockCustomerRepo.findById(customerId))
                .thenReturn(Optional.of(customer))
        ;

        // when
        val token = customerService.login(customerId);

        // then
        assertThat(token).isEqualTo(expectedToken);

        // Make no token is saved to DB for existing customer
        verify(mockCustomerRepo, never()).save(any());
    }

    // ~ getCustomerByToken() ~
    @Test
    void givenValidToken_whenGetCustomerByToken_thenReturnCustomerId() {
        // given
        val customerId = "1122334455";
        val token = "tokentokentoken";

        val customer = generateCustomer(customerId, token);

        when(mockCustomerRepo.findByToken(token))
                .thenReturn(Optional.of(customer));

        // when
        val result = customerService.getCustomerByToken(token);

        // then
        assertThat(result).contains(customerId);
    }

    @Test
    void givenInvalidToken_whenGetCustomerByToken_thenReturnEmpty() {
        // given
        val invalidToken = "invalid";

        when(mockCustomerRepo.findById(invalidToken))
                .thenReturn(Optional.empty());

        // when
        val result = customerService.getCustomerByToken(invalidToken);

        // then
        assertThat(result).isEmpty();
    }

    private Customer generateCustomer(String id, String token) {
        val customer = new Customer();
        customer.setId(id);
        customer.setToken(token);

        return customer;
    }
}
