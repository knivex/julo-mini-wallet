package net.knivex.mini_wallet.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import net.knivex.mini_wallet.model.Customer;
import net.knivex.mini_wallet.repo.CustomerRepo;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

/**
 * Class for saving customer & login information.
 */
@Service
@RequiredArgsConstructor
public class CustomerService {
    private final static String TOKEN_CHAR = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private final static int TOKEN_CHAR_LEN = TOKEN_CHAR.length();

    private final CustomerRepo customerRepo;

    /**
     * Log a customer in.
     *
     * @param customerXid Customer ID.
     *
     * @return Token that can be used for authenticating wallet requests.
     */
    public String login(@NonNull String customerXid) {
        val customer = customerRepo.findById(customerXid);
        if(customer.isPresent())
            return customer.get().getToken();

        // New customer is logging in
        val token = new char[42];
        val rnd = new Random();
        for(int i=0; i<42; i++)
            token[i] = TOKEN_CHAR.charAt(rnd.nextInt(TOKEN_CHAR_LEN));
        val tokenStr = new String(token);

        // Create and save new customer
        val newCustomer = new Customer();
        newCustomer.setId(customerXid);
        newCustomer.setToken(tokenStr);
        customerRepo.save(newCustomer);

        return tokenStr;
    }

    /**
     * Find a logged in customer by token.
     *
     * @param token The token from login method.
     *
     * @return The customer ID if token exists, or {@link Optional#empty()} if token doesn't exist.
     */
    public Optional<String> getCustomerByToken(@NonNull String token) {
        return customerRepo.findByToken(token)
                .map(Customer::getId);
    }
}
