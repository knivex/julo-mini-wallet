package net.knivex.mini_wallet.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import net.knivex.mini_wallet.exception.*;
import net.knivex.mini_wallet.model.DepositTrx;
import net.knivex.mini_wallet.model.Wallet;
import net.knivex.mini_wallet.model.WithdrawalTrx;
import net.knivex.mini_wallet.repo.TransactionRepo;
import net.knivex.mini_wallet.repo.WalletRepo;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

/**
 * Service class which saves and handles wallet logic.
 */
@Service
@RequiredArgsConstructor
public class WalletService {
    private final WalletRepo walletRepo;
    private final TransactionRepo transactionRepo;

    /**
     * Enables a wallet for a customer.
     *
     * @param customerId Customer whose wallet will be enabled.
     *
     * @return Customer's wallet.
     *
     * @throws EnabledWalletException When customer's wallet is already enabled.
     */
    public Wallet enable(@NonNull String customerId) throws EnabledWalletException {
        val walletOpt = walletRepo.findByOwnedBy(customerId);

        if(walletOpt.isPresent()) {
            val wallet = walletOpt.get();

            if(wallet.isEnabled())
                throw new EnabledWalletException("Customer [" +customerId+ "] already has enabled wallet");
            else {
                // Re-enable disabled wallet
                wallet.enable();
                walletRepo.save(wallet);
                return wallet;
            }
        }

        // Create new wallet for customer
        val newWallet = new Wallet();
        newWallet.setOwnedBy(customerId);

        walletRepo.save(newWallet);

        return newWallet;
    }

    /**
     * Disables customer wallet.
     *
     * @param customerId Customer whose wallet will be disabled.
     *
     * @return Customer's disabled wallet.
     *
     * @throws WalletNotFoundException When customer ID cannot be found.
     */
    public Wallet disable(@NonNull String customerId) throws WalletNotFoundException {
        val wallet = walletRepo.findByOwnedBy(customerId)
                .orElseThrow(() -> new WalletNotFoundException("Wallet for customer [" +customerId+ "] cannot be found"))
        ;

        // Only disable active wallet
        if(wallet.isEnabled()) {
            wallet.disable();
            walletRepo.save(wallet);
        }

        return wallet;
    }

    /**
     * Get wallet info for user.
     *
     * @param customerId Customer's ID.
     *
     * @return The wallet info, of {@link Optional#empty()} when no wallet is found for the customer.
     */
    public Optional<Wallet> find(@NonNull String customerId) {
        return walletRepo.findByOwnedBy(customerId);
    }

    /**
     * Deposit a certain amount of money into customer's wallet.
     *
     * @param customerId Customer ID who executes the deposit.
     * @param amount How much money deposited.
     * @param referenceId External reference ID.
     *
     * @return Information about the deposit transaction.
     *
     * @throws WalletNotFoundException When no wallet can be found for the customer.
     * @throws DisabledWalletException When customer is attempting to deposit to a disabled wallet.
     * @throws NegativeAmountException When the deposit amount is negative.
     * @throws DuplicateReferenceException When the reference ID is already used for another transaction.
     */
    public DepositTrx deposit(@NonNull String customerId, long amount, @NonNull UUID referenceId) throws
            WalletNotFoundException,
            DisabledWalletException,
            NegativeAmountException,
            DuplicateReferenceException
    {
        // Validate arguments and states
        if(amount < 0)
            throw new NegativeAmountException("Deposit amount cannot be negative");

        if(transactionRepo.existsByReferenceId(referenceId))
            throw new DuplicateReferenceException("Reference ID [" +referenceId+ "] is already used");

        // Find and validate wallet
        val wallet = walletRepo.findByOwnedBy(customerId)
                .orElseThrow(() -> new WalletNotFoundException("Cannot find wallet for customer [" +customerId+ "]"));
        if( !wallet.isEnabled() )
            throw new DisabledWalletException("Wallet for customer [" +customerId+ "] is disabled");

        // Execute the deposit
        wallet.setBalance(
                wallet.getBalance() + amount
        );

        val depositTrx = new DepositTrx();
        depositTrx.setAmount(amount);
        depositTrx.setReferenceId(referenceId);
        depositTrx.setDepositedBy(customerId);

        walletRepo.save(wallet);
        transactionRepo.save(depositTrx);

        return depositTrx;
    }

    /**
     * Withdraw a certain amount of money from customer wallet.
     *
     * @param customerId Customer ID who executes the withdrawal.
     * @param amount How much money is withdrawn.
     * @param referenceId External reference ID.
     *
     * @return Information about the withdrawal transaction.
     *
     * @throws WalletNotFoundException When no wallet can be found for the customer.
     * @throws DisabledWalletException When customer is attempting to withdraw from a disabled wallet.
     * @throws NegativeAmountException When the withdrawn amount is negative.
     * @throws DuplicateReferenceException When the reference ID is already used for another transaction.
     * @throws AmountExceededException When the withdrawn amount is larger than current customer's balance.
     */
    public WithdrawalTrx withdraw(@NonNull String customerId, long amount, @NonNull UUID referenceId) throws
            WalletNotFoundException,
            DisabledWalletException,
            NegativeAmountException,
            DuplicateReferenceException,
            AmountExceededException
    {
        // Validate arguments and states
        if(amount < 0)
            throw new NegativeAmountException("Withdrawn amount cannot be negative");

        if(transactionRepo.existsByReferenceId(referenceId))
            throw new DuplicateReferenceException("Reference ID [" +referenceId+ "] is already used");

        // Find and validate wallet
        val wallet = walletRepo.findByOwnedBy(customerId)
                .orElseThrow(() -> new WalletNotFoundException("Cannot find wallet for customer [" +customerId+ "]"));
        if( !wallet.isEnabled() )
            throw new DisabledWalletException("Wallet for customer [" +customerId+ "] is disabled");
        if(wallet.getBalance() < amount)
            throw new AmountExceededException("Cannot withdraw [" +amount+ "], wallet only contains [" +wallet.getBalance()+ "]");

        // Execute the withdrawal
        wallet.setBalance(
                wallet.getBalance() - amount
        );

        val withdrawalTrx = new WithdrawalTrx();
        withdrawalTrx.setAmount(amount);
        withdrawalTrx.setReferenceId(referenceId);
        withdrawalTrx.setWithdrawnBy(customerId);

        walletRepo.save(wallet);
        transactionRepo.save(withdrawalTrx);

        return withdrawalTrx;
    }
}
