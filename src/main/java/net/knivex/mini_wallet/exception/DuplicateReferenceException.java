package net.knivex.mini_wallet.exception;

/**
 * Exception when a reference ID already exists in system.
 */
public class DuplicateReferenceException extends Exception {
    public DuplicateReferenceException(String message) {
        super(message);
    }
}
