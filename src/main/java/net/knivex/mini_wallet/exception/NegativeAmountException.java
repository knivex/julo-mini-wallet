package net.knivex.mini_wallet.exception;

/**
 * Exception when monetary value is negative.
 */
public class NegativeAmountException extends Exception {
    public NegativeAmountException(String message) {
        super(message);
    }
}
