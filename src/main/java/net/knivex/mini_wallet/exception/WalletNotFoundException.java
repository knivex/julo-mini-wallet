package net.knivex.mini_wallet.exception;

/**
 * Exception when no wallet is found for a customer.
 */
public class WalletNotFoundException extends Exception {
    public WalletNotFoundException(String message) {
        super(message);
    }
}
