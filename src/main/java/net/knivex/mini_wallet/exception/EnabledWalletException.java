package net.knivex.mini_wallet.exception;

/**
 * Exception when enabled wallet is re-enabled.
 */
public class EnabledWalletException extends Exception {
    public EnabledWalletException(String message) {
        super(message);
    }
}
