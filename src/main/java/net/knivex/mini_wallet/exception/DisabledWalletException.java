package net.knivex.mini_wallet.exception;

/**
 * Exception when action is performed on a disabled wallet.
 */
public class DisabledWalletException extends Exception {
    public DisabledWalletException(String message) {
        super(message);
    }
}
