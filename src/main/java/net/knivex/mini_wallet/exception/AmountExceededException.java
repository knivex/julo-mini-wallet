package net.knivex.mini_wallet.exception;

/**
 * Exception when an amount is over a certain threshold.
 */
public class AmountExceededException extends Exception {
    public AmountExceededException(String message) {
        super(message);
    }
}
