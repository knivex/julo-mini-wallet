package net.knivex.mini_wallet.repo;

import net.knivex.mini_wallet.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface TransactionRepo extends JpaRepository<Transaction, UUID> {
    boolean existsByReferenceId(UUID referenceId);
}
