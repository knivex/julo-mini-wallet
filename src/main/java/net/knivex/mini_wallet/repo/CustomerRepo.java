package net.knivex.mini_wallet.repo;

import net.knivex.mini_wallet.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, String> {
    Optional<Customer> findByToken(String token);
}
