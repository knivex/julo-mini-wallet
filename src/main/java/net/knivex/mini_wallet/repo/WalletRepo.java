package net.knivex.mini_wallet.repo;

import net.knivex.mini_wallet.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WalletRepo extends JpaRepository<Wallet, String> {
    Optional<Wallet> findByOwnedBy(String customerId);
}
