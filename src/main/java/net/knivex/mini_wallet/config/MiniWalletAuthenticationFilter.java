package net.knivex.mini_wallet.config;

import lombok.val;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class that responsible to extract token and authenticate it.
 */
public class MiniWalletAuthenticationFilter extends AbstractAuthenticationProcessingFilter
    implements AuthenticationFailureHandler
{
    private final static String TOKEN_PREFIX = "Token";

    private final HandlerExceptionResolver handlerExceptionResolver;

    public MiniWalletAuthenticationFilter(
            AuthenticationManager authenticationManager,
            RequestMatcher requestMatcher,
            HandlerExceptionResolver handlerExceptionResolver
    ) {
        super(requestMatcher);
        setAuthenticationManager(authenticationManager);
        setAuthenticationFailureHandler(this);

        this.handlerExceptionResolver = handlerExceptionResolver;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        // Extract and validate auth header
        val tokenHeader = request.getHeader("Authorization");
        if(tokenHeader == null)
            throw new InsufficientAuthenticationException("Missing authorization header");
        if(!tokenHeader.startsWith(TOKEN_PREFIX))
            throw new InsufficientAuthenticationException("Invalid token prefix");

        // Extract and return token
        val token = tokenHeader.substring(TOKEN_PREFIX.length()).trim();
        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(token, token)
        );
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(authResult);
        chain.doFilter(request, response);
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        handlerExceptionResolver.resolveException(request, response, null, exception);
    }
}
