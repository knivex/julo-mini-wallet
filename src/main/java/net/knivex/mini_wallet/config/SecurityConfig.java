package net.knivex.mini_wallet.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.servlet.HandlerExceptionResolver;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final String INIT_PATH = "/api/v1/init";
    private final String H2_PATH = "/h2-console/**";
    private final RequestMatcher PROTECTED_MATCHER = new NegatedRequestMatcher(new OrRequestMatcher(
            new AntPathRequestMatcher(INIT_PATH),
            new AntPathRequestMatcher(H2_PATH)
    ));

    private final MiniWalletAuthenticationProvider authenticationProvider;
    private final HandlerExceptionResolver handlerExceptionResolver;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
        .and().authorizeRequests()
                .mvcMatchers(INIT_PATH).permitAll()
                .antMatchers(H2_PATH).permitAll()
                .anyRequest().authenticated()
        .and()
                .headers().frameOptions().disable()
        .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(miniWalletAuthenticationFilter(), AnonymousAuthenticationFilter.class)
                .csrf().disable()
                .formLogin().disable()
                .logout().disable()
        ;
    }

    @Bean
    public MiniWalletAuthenticationFilter miniWalletAuthenticationFilter() throws Exception {
        return new MiniWalletAuthenticationFilter(authenticationManager(), PROTECTED_MATCHER, handlerExceptionResolver);
    }
}
