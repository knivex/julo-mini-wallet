package net.knivex.mini_wallet.config;

import lombok.RequiredArgsConstructor;
import lombok.val;
import net.knivex.mini_wallet.service.CustomerService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * Saves the logic for authenticating users.
 */
@Component
@RequiredArgsConstructor
public class MiniWalletAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    private final CustomerService customerService;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        // noop
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        // Find user with specified token
        val token = (String)usernamePasswordAuthenticationToken.getPrincipal();
        val user = customerService.getCustomerByToken(token)
                .orElseThrow(() -> new UsernameNotFoundException("Token " +token+ " doesn't exist"))
        ;

        return new User(user, "password", Collections.emptyList());
    }
}
