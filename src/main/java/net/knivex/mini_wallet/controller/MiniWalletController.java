package net.knivex.mini_wallet.controller;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.RequiredArgsConstructor;
import lombok.val;
import net.knivex.mini_wallet.dto.*;
import net.knivex.mini_wallet.exception.*;
import net.knivex.mini_wallet.model.*;
import net.knivex.mini_wallet.service.CustomerService;
import net.knivex.mini_wallet.service.WalletService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * Endpoints for mini wallet URLs.
 */
@RestController
@RequestMapping("/api/v1/")
@RequiredArgsConstructor
public class MiniWalletController {
    private final CustomerService customerService;
    private final WalletService walletService;

    @PostMapping("init")
    public ResponseEntity<JSend<?>> init(@RequestParam(name="customer_xid") String customerXid) {
        // Validate customer ID
        if(customerXid == null || customerXid.isEmpty())
            return ResponseEntity.badRequest()
                    .body(JSend.fail(
                            InvalidParamError.of("customer_xid")
                    ));

        // Log the customer in
        val token = customerService.login(customerXid);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(JSend.success(
                        new Token(token)
                ));
    }

    @PostMapping("wallet")
    @JsonView(Wallet.View.Enable.class)
    public ResponseEntity<JSend<?>> enableMyWallet(Authentication authentication) {
        try {
            // Enable customer's wallet
            val wallet = walletService.enable(authentication.getName());
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(JSend.success(wallet));
        } catch (EnabledWalletException e) {
            return ResponseEntity.badRequest()
                    .body(JSend.fail(
                            new GeneralError(e.getMessage())
                    ));
        }
    }

    @PatchMapping("wallet")
    @JsonView(Wallet.View.Disable.class)
    public ResponseEntity<JSend<?>> disableMyWallet(Authentication auth, @RequestParam("is_disabled") String isDisabled) {
        // Validate parameter
        if( !isDisabled.equals("true") )
            return ResponseEntity.badRequest()
                    .body(JSend.fail(
                            InvalidParamError.of("is_disabled")
                    ));

        try {
            // Disable customer's wallet
            val disabledWallet = walletService.disable(auth.getName());
            return ResponseEntity.status(HttpStatus.OK)
                    .body(JSend.success(disabledWallet));
        } catch (WalletNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(JSend.fail(
                            new GeneralError(e.getMessage())
                    ));
        }
    }

    @GetMapping("wallet")
    @JsonView(Wallet.View.Enable.class)
    public ResponseEntity<JSend<?>> viewMyWalletBalance(Authentication auth) {
        val customerId = auth.getName();

        // Get and validate customer's wallet
        val walletOpt = walletService.find(customerId);
        String errStr = null;

        if( !walletOpt.isPresent() )
            errStr = "Cannot find wallet for customer [" +customerId+ "]";
        else if( !walletOpt.get().isEnabled() )
            errStr = "The wallet for customer [" +customerId+ "] is disabled";

        if(errStr != null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(JSend.fail(
                            new GeneralError(errStr)
                    ));

        return ResponseEntity.ok( JSend.success(walletOpt.get()) );
    }

    @PostMapping("wallet/deposits")
    public ResponseEntity<JSend<?>> addVirtualMoneyToMyWallet(
            Authentication auth,
            @RequestParam(name="amount") long amount,
            @RequestParam(name="reference_id") UUID referenceId
    ) {
        try {
            val depositTrx = walletService.deposit(auth.getName(), amount, referenceId);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(JSend.success(depositTrx));

        } catch (WalletNotFoundException | DisabledWalletException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(JSend.fail( new GeneralError(e.getMessage()) ));

        } catch (NegativeAmountException | DuplicateReferenceException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(JSend.fail( new GeneralError(e.getMessage()) ));
        }
    }

    @PostMapping("wallet/withdrawals")
    public ResponseEntity<JSend<?>> useVirtualMoneyFromMyWallet(
            Authentication auth,
            @RequestParam(name="amount") long amount,
            @RequestParam(name="reference_id") UUID referenceId
    ) {
        try {
            val withdrawTrx = walletService.withdraw(auth.getName(), amount, referenceId);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(JSend.success(withdrawTrx));

        } catch (WalletNotFoundException | DisabledWalletException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(JSend.fail( new GeneralError(e.getMessage()) ));

        } catch (NegativeAmountException | AmountExceededException | DuplicateReferenceException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(JSend.fail( new GeneralError(e.getMessage()) ));
        }
    }
}
