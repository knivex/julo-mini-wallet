package net.knivex.mini_wallet.controller;

import lombok.extern.slf4j.Slf4j;
import net.knivex.mini_wallet.dto.GeneralError;
import net.knivex.mini_wallet.dto.InvalidParamError;
import net.knivex.mini_wallet.dto.JSend;
import net.knivex.mini_wallet.dto.MissingParamError;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * Error handlers for REST endpoints.
 */
@RestControllerAdvice
@Slf4j
public class ControllerAdvice {
    // ~ Error handlers ~
    @ExceptionHandler
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public JSend<GeneralError> onUnauthorized(BadCredentialsException e) {
        return JSend.fail(
                new GeneralError("Invalid token")
        );
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public JSend<GeneralError> onMissingAuthHeader(InsufficientAuthenticationException e) {
        return JSend.fail(
                new GeneralError(e.getMessage())
        );
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JSend<MissingParamError> onMissingParam(MissingServletRequestParameterException ex) {
        return JSend.fail(
                MissingParamError.of(ex.getParameterName())
        );
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JSend<InvalidParamError> onInvalidFormat(MethodArgumentTypeMismatchException e) {
        return JSend.fail(
                InvalidParamError.of(e.getName(), e.getParameter().getParameterType())
        );
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public JSend<GeneralError> onGeneralError(Exception ex) {
        log.error("INTERNAL SERVER ERROR", ex);
        return JSend.error(new GeneralError(
                ex.getClass().getName() +" : "+ ex.getMessage()
        ));
    }
}
