package net.knivex.mini_wallet.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Customer {
    @Id
    private String id;

    @Column(unique = true)
    private String token;
}
