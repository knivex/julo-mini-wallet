package net.knivex.mini_wallet.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import java.time.OffsetDateTime;

/**
 * Data class which save information about a withdrawal transaction.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class WithdrawalTrx extends Transaction {
    public WithdrawalTrx() {
        super(TrxType.withdrawal);
    }

    private String withdrawnBy;

    private OffsetDateTime withdrawnAt = OffsetDateTime.now();
}
