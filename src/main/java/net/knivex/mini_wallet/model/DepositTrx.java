package net.knivex.mini_wallet.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import java.time.OffsetDateTime;

/**
 * Data class which save information about a deposit transaction.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class DepositTrx extends Transaction {
    public DepositTrx() {
        super(TrxType.deposit);
    }

    private String depositedBy;

    private OffsetDateTime depositedAt = OffsetDateTime.now();
}
