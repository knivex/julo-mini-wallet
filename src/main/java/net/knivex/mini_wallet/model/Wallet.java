package net.knivex.mini_wallet.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Class that saves wallet information.
 */
@Data
@Entity
public class Wallet {
    public enum WalletStatus {
        enabled,
        disabled,
    }

    /**
     * Controls which field will be shown on output JSON.
     */
    public interface View {
        interface Enable {}
        interface Disable {}
    }

    @Id
    private UUID id = UUID.randomUUID();

    @Column(unique = true)
    private String ownedBy;

    @Enumerated(EnumType.STRING)
    private WalletStatus status = WalletStatus.enabled;

    @JsonView(View.Enable.class)
    private OffsetDateTime enabledAt = OffsetDateTime.now();
    @JsonView(View.Disable.class)
    private OffsetDateTime disabledAt = null;

    private long balance = 0L;

    @JsonIgnore
    public boolean isEnabled() {
        return status == WalletStatus.enabled;
    }

    public void enable() {
        status = WalletStatus.enabled;
        enabledAt = OffsetDateTime.now();
    }

    public void disable() {
        status = WalletStatus.disabled;
        disabledAt = OffsetDateTime.now();
    }
}
