package net.knivex.mini_wallet.model;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;
import java.util.UUID;

/**
 * Base model for transactions.
 */
@Data
@Entity
@DiscriminatorColumn(
        name = "type",
        discriminatorType = DiscriminatorType.STRING
)
@Table(indexes = {
        @Index(columnList = "type")
})
public abstract class Transaction {
    public enum TrxType {
        deposit,
        withdrawal,
    }

    @SuppressWarnings("unused")
    public Transaction() {
        this(TrxType.deposit);
    }

    public Transaction(@NonNull TrxType type) {
        this.type = type;
    }

    @Id
    private UUID id = UUID.randomUUID();

    private String status = "success";

    private long amount;

    @Column(unique = true)
    private UUID referenceId;

    @Column(nullable = false, insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private TrxType type;
}
