package net.knivex.mini_wallet.dto;

import lombok.Getter;
import lombok.NonNull;

import java.util.HashMap;
import java.util.UUID;

/**
 * Class for storing info about invalid parameter format.
 */
@Getter
public class InvalidParamError {
    private final HashMap<String, String[]> error = new HashMap<>();

    private InvalidParamError(@NonNull String paramName, Class<?> paramType) {
        String err;
        if(paramType == UUID.class)
            err = "Invalid UUID format";
        else
            err = "Invalid format";

        error.put(paramName, new String[]{err});
    }

    public static InvalidParamError of(String paramName, Class<?> paramType) {
        return new InvalidParamError(paramName, paramType);
    }

    public static InvalidParamError of(String paramName) {
        return new InvalidParamError(paramName, null);
    }
}
