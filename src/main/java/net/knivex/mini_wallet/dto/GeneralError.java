package net.knivex.mini_wallet.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Class for saving information about error.
 */
@Getter
@AllArgsConstructor
public class GeneralError {
    private String error;
}
