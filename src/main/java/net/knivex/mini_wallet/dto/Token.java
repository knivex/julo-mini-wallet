package net.knivex.mini_wallet.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Data class which contains login token.
 */
@Data
@AllArgsConstructor
public class Token {
    private String token;
}
