package net.knivex.mini_wallet.dto;

import lombok.Getter;
import lombok.NonNull;

import java.util.HashMap;

/**
 * Class for storing info about missing parameter.
 */
@Getter
public class MissingParamError {
    private final HashMap<String, String[]> error = new HashMap<>();

    private MissingParamError(@NonNull String paramName) {
        error.put(paramName, new String[]{"Missing data for required field."});
    }

    public static MissingParamError of(String paramName) {
        return new MissingParamError(paramName);
    }
}

