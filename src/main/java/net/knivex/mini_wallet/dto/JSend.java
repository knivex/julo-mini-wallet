package net.knivex.mini_wallet.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

/**
 * Wrapper class for response in JSend format.
 */
@AllArgsConstructor
@Getter
public class JSend<D> {
    private String status;
    private D data;

    public static <D> JSend<D> success(@NonNull D data) {
        return new JSend<>("success", data);
    }

    public static <D> JSend<D> fail(@NonNull D data) {
        return new JSend<>("fail", data);
    }

    public static <D> JSend<D> error(@NonNull D data) {
        return new JSend<>("error", data);
    }
}
